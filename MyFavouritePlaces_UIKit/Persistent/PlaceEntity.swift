//
//  PlaceEntity.swift
//  MyFavouritePlaces_UIKit
//
//  Created by Mahmoud HodaeeNia on 11/25/21.
//

import Foundation
import CoreData
import UIKit
import GoogleMaps

class PlaceEntity: NSManagedObject {
    static var viewContext: NSManagedObjectContext {
        (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    }

    lazy var gmsMarker: GMSMarker = {
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: CLLocationDegrees(self.latitude), longitude: CLLocationDegrees(self.longitude)))
        marker.title = (self.friends?.allObjects as? [FriendEntity])?.reduce("", { $0 + ($1.first_name ?? "") + ($1.last_name ?? "") })
        return marker
    }()
}

