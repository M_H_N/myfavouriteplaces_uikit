//
//  ViewControllerFriendsSelection.swift
//  MyFavouritePlaces_UIKit
//
//  Created by Mahmoud HodaeeNia on 11/25/21.
//

import UIKit
import CoreData
import GoogleMaps

class ViewControllerFriendsSelection: UIViewController, UITableViewDataSource, UITableViewDelegate {

    static func getInstance(location: CLLocationCoordinate2D) -> ViewControllerFriendsSelection {
        let vc = ViewControllerFriendsSelection(nibName: "ViewControllerFriendsSelection", bundle: nil)
        vc.selectedLocation = location
        return vc
    }


    @IBOutlet weak var tableViewFriends: UITableView! {
        didSet {
            let nib = UINib(nibName: TableViewCellFriend.REUSE_ID, bundle: nil)
            self.tableViewFriends.register(nib, forCellReuseIdentifier: TableViewCellFriend.REUSE_ID)

            self.tableViewFriends.allowsMultipleSelection = true
            self.tableViewFriends.allowsSelection = true
        }
    }

    @IBOutlet weak var btnAddNewFriend: UIButton! {
        didSet {
            self.btnAddNewFriend.layer.cornerRadius = self.btnAddNewFriend.bounds.width / 2
        }
    }

    internal weak var parentVc: ViewControllerAddNewPin? = nil
    private var selectedLocation: CLLocationCoordinate2D!
    private var friends = [FriendEntity]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadFriends()
        // Do any additional setup after loading the view.
    }

    private func loadFriends() {
        let context = FriendEntity.viewContext
        let request: NSFetchRequest<FriendEntity> = FriendEntity.fetchRequest()
        context.perform { [weak self] in
            if let result = try? context.fetch(request) {
                self?.friends = result
                self?.tableViewFriends.reloadData()
            }
        }
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.friends.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewCellFriend.REUSE_ID, for: indexPath) as! TableViewCellFriend
        let friend = self.friends[indexPath.row]
        cell.lblTitle.text = "\(friend.first_name ?? "") \(friend.last_name ?? "")"
        return cell
    }

    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            guard indexPath.row >= 0, indexPath.row < self.friends.count else {
                return
            }

            let context = FriendEntity.viewContext
            context.performAndWait {
                context.delete(self.friends[indexPath.row])
                try? context.save()
            }


            self.friends.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .automatic)
            self.parentVc?.parentVC?.loadFriends()
        }
    }

    private func saveNewFriend(firstName: String, lastName: String) {
        let context = FriendEntity.viewContext
        let entity = NSEntityDescription.insertNewObject(forEntityName: "FriendEntity", into: context) as! FriendEntity
        entity.first_name = firstName
        entity.last_name = lastName
        context.performAndWait {
            context.insert(entity)
            try? context.save()
        }
        self.loadFriends()
    }

    @IBAction func btnAddNewFriendClicked(_ sender: UIButton) {
        let alert = UIAlertController(title: "Add New Friend", message: "Enter your friend's information", preferredStyle: .alert)
        alert.addTextField { txt in
            txt.placeholder = "Name"
        }
        alert.addTextField { txt in
            txt.placeholder = "Family"
        }

        alert.addAction(UIAlertAction(title: "Add", style: .default) { [weak self] action in
            if alert.textFields?.allSatisfy({ !($0.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty ?? true) }) ?? false {
                self?.saveNewFriend(firstName: alert.textFields!.first!.text!, lastName: alert.textFields![1].text!)
                alert.dismiss(animated: true)
                self?.parentVc?.parentVC?.loadFriends()
            }
        })

        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel) { action in
            alert.dismiss(animated: true)
        })

        self.present(alert, animated: true)
    }

    @IBAction func btnSaveClicked(_ sender: UIButton) {
        let selectedFriends = self.tableViewFriends.indexPathsForSelectedRows?
                .map({ (indexPath: IndexPath) in self.friends[indexPath.row] })

        guard let selectedFriends = selectedFriends, !selectedFriends.isEmpty else {
            return
        }

        // Save the date into the database
        let context = PlaceEntity.viewContext
        let place = NSEntityDescription.insertNewObject(forEntityName: "PlaceEntity", into: context) as! PlaceEntity
        place.latitude = Float(self.selectedLocation.latitude)
        place.longitude = Float(self.selectedLocation.longitude)
        selectedFriends.forEach({ place.addToFriends($0) })


        context.performAndWait {
            context.insert(place)
            try? context.save()
        }

        self.dismiss(animated: true)
        self.parentVc?.dismiss(animated: true)
    }
}
