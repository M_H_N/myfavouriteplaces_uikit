//
//  ViewControllerAddNewPin.swift
//  MyFavouritePlaces_UIKit
//
//  Created by Mahmoud HodaeeNia on 11/25/21.
//

import UIKit
import GoogleMaps

class ViewControllerAddNewPin: UIViewController {
    static func getInstance(location: CLLocationCoordinate2D) -> ViewControllerAddNewPin {
        let vc = ViewControllerAddNewPin(nibName: "ViewControllerAddNewPin", bundle: nil)
        vc.initialCameraTarget = location
        return vc
    }

    private var mapView: GMSMapView!
    @IBOutlet weak var viewContainerMap: UIView! {
        didSet {
            let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
            self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
            self.viewContainerMap.addSubview(mapView)
        }
    }

    @IBOutlet weak var imgStaticMarker: UIImageView! {
        didSet {
            self.imgStaticMarker.image = GMSMarker.markerImage(with: nil)
        }
    }

    internal weak var parentVC: ViewController? = nil
    private var initialCameraTarget: CLLocationCoordinate2D? = nil

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        if let target = self.initialCameraTarget {
            self.mapView.camera = GMSCameraPosition.camera(withTarget: target, zoom: 6.0)
        }
    }

    @IBAction func btnNextClicked(_ sender: UIButton) {
        let selectedLocation = self.mapView.camera.target
        let vc = ViewControllerFriendsSelection.getInstance(location: selectedLocation)
        vc.parentVc = self
        self.present(vc, animated: true)
    }

    @IBAction func btnCloseClicked(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
