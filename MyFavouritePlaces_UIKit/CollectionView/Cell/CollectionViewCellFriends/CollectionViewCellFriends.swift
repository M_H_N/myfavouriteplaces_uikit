//
//  CollectionViewCellFriends.swift
//  MyFavouritePlaces_UIKit
//
//  Created by Mahmoud HodaeeNia on 11/25/21.
//

import UIKit

class CollectionViewCellFriends: UICollectionViewCell {

    static let REUSE_ID = "CollectionViewCellFriends"


    @IBOutlet weak var viewContainerRoot: UIView! {
        didSet {
            self.viewContainerRoot.clipsToBounds = true
            self.viewContainerRoot.layer.cornerRadius = 5
        }
    }

    @IBOutlet weak var lblFullName: UILabel! {
        didSet {
//            self.lblFullName.layer.cornerRadius = self.lblFullName.bounds.height / 2
        }
    }
    override var isSelected: Bool {
        get {
            super.isSelected
        }
        set {
            super.isSelected = newValue
            if newValue {
                self.viewContainerRoot.backgroundColor = UIColor.orange
            } else {
                self.viewContainerRoot.backgroundColor = UIColor.opaqueSeparator
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
