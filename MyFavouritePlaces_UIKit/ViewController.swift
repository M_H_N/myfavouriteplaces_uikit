//
//  ViewController.swift
//  MyFavouritePlaces_UIKit
//
//  Created by Mahmoud HodaeeNia on 11/25/21.
//

import UIKit
import GoogleMaps
import CoreData

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    var mapView: GMSMapView!
    @IBOutlet weak var viewContainerMap: UIView! {
        didSet {
            let camera = GMSCameraPosition.camera(withLatitude: -33.86, longitude: 151.20, zoom: 6.0)
            self.mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
            self.viewContainerMap.addSubview(mapView)
        }
    }

    @IBOutlet weak var btnAddNewPin: UIButton! {
        didSet {
            self.btnAddNewPin.layer.cornerRadius = self.btnAddNewPin.bounds.width / 2
        }
    }

    @IBOutlet weak var collectionViewFriends: UICollectionView! {
        didSet {
            let itemSize = NSCollectionLayoutSize(widthDimension: .estimated(50), heightDimension: .estimated(50))
            let item = NSCollectionLayoutItem(layoutSize: itemSize)

            let group = NSCollectionLayoutGroup.vertical(layoutSize: itemSize, subitems: [item])

            let section = NSCollectionLayoutSection(group: group)
            section.interGroupSpacing = 5.0
            section.orthogonalScrollingBehavior = .continuousGroupLeadingBoundary

            let layout = UICollectionViewCompositionalLayout(section: section)
            self.collectionViewFriends.collectionViewLayout = layout


            let nib = UINib(nibName: CollectionViewCellFriends.REUSE_ID, bundle: nil)
            self.collectionViewFriends.register(nib, forCellWithReuseIdentifier: CollectionViewCellFriends.REUSE_ID)
        }
    }


    private var friends = [FriendEntity]() {
        didSet {
            self.collectionViewFriends.reloadData()
        }
    }


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.loadFriends()
    }


    internal func loadFriends() {
        let context = FriendEntity.viewContext
        let request: NSFetchRequest<FriendEntity> = FriendEntity.fetchRequest()
        context.perform { [weak self] in
            if let result = try? context.fetch(request) {
                self?.friends = result
            }
        }
    }

    @IBAction func btnAddNewPinClicked(_ sender: UIButton) {
        let coordinate = self.mapView.camera.target
        let vc = ViewControllerAddNewPin.getInstance(location: coordinate)
        vc.parentVC = self
        self.present(vc, animated: true)
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        self.friends.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CollectionViewCellFriends.REUSE_ID, for: indexPath) as! CollectionViewCellFriends
        let friend = self.friends[indexPath.row]
        cell.lblFullName.text = "\(friend.first_name ?? "") \(friend.last_name ?? "")"
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.mapView.clear()

        let friend = self.friends[indexPath.row]

        let locations = (friend.places?.allObjects as? [PlaceEntity])

        let markers = locations?.map({ $0.gmsMarker })
        markers?.forEach({ $0.map = self.mapView })

        if let locations = locations, !locations.isEmpty {
            let minOfLats = CLLocationDegrees(locations.compactMap({ $0.latitude }).min()!)
            let maxOfLats = CLLocationDegrees(locations.map({ $0.latitude }).max()!)

            let minOfLongs = CLLocationDegrees(locations.map({ $0.longitude }).min()!)
            let maxOfLongs = CLLocationDegrees(locations.map({ $0.longitude }).max()!)

            let minLocation = CLLocationCoordinate2D(latitude: minOfLats, longitude: minOfLongs)
            let maxLocation = CLLocationCoordinate2D(latitude: maxOfLats, longitude: maxOfLongs)

            let bounds = GMSCoordinateBounds(coordinate: minLocation, coordinate: maxLocation)
            let cameraUpdate: GMSCameraUpdate = GMSCameraUpdate.fit(bounds, withPadding: 50.0)
            self.mapView.animate(with: cameraUpdate)
        }

    }
}

